package domain

sealed trait Availability {
  val startDate: Int;
  val endDate: Int;
}
