package assessment

import java.util.Date
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor

import domain.schedule._
import io._

import scala.util.Try
import scala.xml.{Elem, NodeSeq, XML}

object AssessmentMS01 extends Schedule {
  // TODO: Use the functions in your own code to implement the assessment of ms01
  def create(xml: Elem): Try[Elem] = ???

  def createTest(xml:Elem): Any = {
    val duration = xml \ "@duration";

    val a = (xml \\ "viva").map(
      //for each viva
      viva=>(viva.child\\"_").map(
        //find and create Personal from id for each element of viva
        personal => gatherAndCreatePersonal((xml \\ "resources"), personal.head.label, (personal \\ "@id").toString())
      )
    )
  }

  def gatherAndCreatePersonal(seq: NodeSeq, role: String, id: String): Personal = {
    val elem = (seq\\"_").filter(
      x=> (x \\ "@id").toString() == id
    );

    val availability: Seq[Availability] = handleAvailability((elem \\ "availability"));

    new Personal((elem\\"@id").toString(), role, (elem \\ "@name").toString(), availability);
  }

  def handleAvailability(seq: NodeSeq): Seq[Availability] = {
    seq.map(
      x => new Availability (parseTime((x \\ "@start").toString()),
        parseTime((x \\ "@end").toString()),
        (x \\ "@preference").toString().toInt
    )
    );
  }

  def parseTime(time: String): TemporalAccessor = {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd['T']HH:mm:ss");
    formatter.parse(time)
  }

}

object AssessmentMS03 extends Schedule {
  // TODO: Use the functions in your own code to implement the assessment of ms03
  def create(xml: Elem): Try[Elem] = ???
}

class Personal (val id: String, val role: String, val name: String, val availability: Seq[Availability])

class Availability (val start: TemporalAccessor, val end: TemporalAccessor, val preference: Integer)

class Viva(val student: String, val title: String, val start: Date, val end: Date, val preference: Int, val president: Personal, val adviser: Personal, val supervisor: Personal)

object Main extends App {
  System.out.println("Absolute path for your agenda xml?");
  val path = scala.io.StdIn.readLine();
  val ioFile = io.FileIO.load(path);
  val agenda = ioFile.get;
  AssessmentMS01.createTest(agenda);
}
